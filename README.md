This repo is a collection of licensing tools. Please refer to READMEs in sub-folders.

### CLA Assistant
[CLA Assistant](./cla-assistant/README.md)

### Acknowledgements
The [CLA Assistant for GitHub](https://cla-assistant.io/) inspired us to create this GitLab CLA Assistant bot, though we ended up taking a different approach to the problem.

This effort is part of a project that has received funding from the European Union’s Horizon 2020 research and innovation program under grant agreement No 871932 delivered through our participation in the eSSIF-Lab, which aims to advance the broad adoption of self-sovereign identity for the benefit of all.

