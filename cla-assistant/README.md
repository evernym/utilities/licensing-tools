This is a minimal tool to organize acceptance of a Contributor License Agreement (CLA) for a GitLab repository.

The solution is based on AWS Lambda.

### Flow description
1. All repositories should enforce a setting “Merge checks -> All discussion must be resolved”
1. Some simple external service (e.g. AWS Lambda) will be triggered by the merge request webhook and will initiate a discussion thread.
   Possible text is 
   “This repository is under a specific license,
   and you must accept the contributor license agreement.
   Please find the main repo licence text in the LICENCE file in the root of the repo,
   and the contributor license agreement text in the CLA file.
   Mark this thread as resolved if you accept both of them.
   Otherwise, this MR and the contribution can not be accepted.”
1. The contributor marks the thread as resolved, so the MR is unblocked to merge


### Steps to set up

#### AWS Secrets Manager
We need to keep our gitlab tokens somehow and AWS Secrets Manager can help with it. All we need - just create new secret.
For it:
1. Go to the `AWS Secrets Manager`  and start `Store a new secret` activity.
   1.1 Choose `Other type of secrets` and add 2 rows on the `Specify the key/value pairs to be stored in this secret` section.
       We need to setup 2 auth tokens here, `X-GitLab-Token` and `GitLab-API-Token`. 
       Those tokens are coming from Gitlab side and can be created on personal page in "Acess Tokens" section. Also, please make sure, that `GitLab-API-Token` has write_access.
   1.2 Set up  `Secret name` and `Description`. For example, name can be as `gitlab/cla`.


#### AWS Lambda actions.
1. Go to the `Lambda` creation page and create a new one, for example it can be named as `CLA`.
   Choose "Author from scratch", setup "Function name" and let's "Runtime" section would be `Python 3.6`. Press "Create function" button.
   
2. On the function view page press `Add trigger` button and setup it:
   2.1 Choose `API Gateway`.
   2.2 For API section choose `Create an API`
   2.3 `API type` should be "HTTP API" and security is `Open`

3. Go to the `Configuration` tab section on the general our lambda page and press on current `Role name`
   3.1 Press `Attach policies` button and choose `SecretsManagerReadWrite` and `Attach policy` button then.

4. Go to the `Code` tab section and copy-paste `lambda_function.py` file and press `Deploy` button

5. Go to the `Configuration -> Environment variables` and add new variable `SECRET_ID = gitlab/cla`


#### Gitlab project setting
On the Gitlab side we need to create a webhook on "Merge request" event and setup url with auth token
`URL` it's a `API endpoint` and can be found on the `API Gateway` details page for our previously created Lambda.
`Secret token` - it's our `X-GitLab-Token`.

Also, in the repo settings enforce `Merge checks` -> `All discussion must be resolved`



### Terraform apply

There are terraform templates for applying on AWS in directory `terraform`. Steps for preparetion are the same as described in terraform documentation.
Only this steps are needed:
* `aws configure` for getting acces to corresponding aws account
* Go to the `terraform` directory
* Run `terrafom init`

After that there are 2 ways for starting deploy process. 
Because of using secrets we have to setup Gitlab access token in hidden mode. For making it there are 2 ways for passing tokens to the terraform:
* Set up environment variables.
```
export TF_VAR_gitlab_api_token=<gitlab-api-token>
export TF_VAR_x_gitlab_token=<x-gitlab-token>
terraform apply
```
* Passing variables directly to terraform
```
terraform apply -var="gitlab_api_token=<gitlab-api-token>" -var="x_gitlab_token=<x-gitlab-token>"
```
