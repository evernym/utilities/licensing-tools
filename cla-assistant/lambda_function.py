import json
import os
import boto3
import urllib3

X_GITLAB_TOKEN_KEY = 'X-GitLab-Token'
GITLAB_API_TOKEN_KEY = 'GitLab-API-Token'
USER_NOTIFICATION_TXT_TEMPLATE = \
  "Hello @{}. " \
  "This repository is under a specific license, and you must accept the contributor license agreement. " \
  "Please find the main repo licence text in the [LICENSE file](./LICENSE.txt) in the root of the repo, " \
  "and the contributor license agreement text [at the link]"\
  "(https://gitlab.com/evernym/docs/general-docs/-/blob/main/Collaboration/EvCLA.txt). " \
  "Mark this thread as resolved if you accept both of them. " \
  "Otherwise this MR and the contribution can not be accepted."

secret_id = os.environ['SECRET_ID']


def lambda_handler(event, _context):
    session = boto3.session.Session()
    region_name = session.region_name
    sm_client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    secrets_resp = sm_client.get_secret_value(
        SecretId=secret_id
    )
    secrets_str = secrets_resp['SecretString']
    secrets_json = json.loads(secrets_str)

    # Get Case insensitive headers map
    event_headers = {k.lower(): v for (k, v) in event["headers"].items()} if "headers" in event else {}

    x_gitlab_token = secrets_json[X_GITLAB_TOKEN_KEY]
    if (X_GITLAB_TOKEN_KEY.lower() not in event_headers
            or event_headers[X_GITLAB_TOKEN_KEY.lower()] != x_gitlab_token):
        print("Auth failed")
        return {
            "statusCode": 401,
            "body": "GitLab Token is missed or incorrect"
        }

    gitlab_event = json.loads(event['body'])

    if gitlab_event['object_kind'] != "merge_request" or gitlab_event['event_type'] != "merge_request":
        print("Not a merge request")
        return {
            "statusCode": 200
        }

    print(gitlab_event)
    mr_attributes = gitlab_event['object_attributes']
    mr_state = mr_attributes['state']
    mr_action = mr_attributes['action']

    if mr_state != "opened" or mr_action != "open":
        print("Not a new merge request")
        return {
            "statusCode": 200
        }

    project_id = gitlab_event['project']['id']
    user = gitlab_event['user']['username']
    mr_iid = mr_attributes['iid']
    # TODO input validation

    http = urllib3.PoolManager()
    gitlab_api_token = secrets_json[GITLAB_API_TOKEN_KEY]
    headers = {"PRIVATE-TOKEN": gitlab_api_token}
    params = {"body": USER_NOTIFICATION_TXT_TEMPLATE.format(user)}
    url = "https://gitlab.com/api/v4/projects/{}/merge_requests/{}/discussions".format(project_id, mr_iid)

    print("Posting the notification to the URL: {}".format(url))
    resp = http.request('POST', url, headers=headers, fields=params)
    print("GitLab response: {}\n{}".format(resp.status, resp.data))

    return {
        'statusCode': resp.status  # TODO analyze the response
    }
