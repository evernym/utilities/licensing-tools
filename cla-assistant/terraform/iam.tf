# IAM
resource "aws_iam_role" "cla_terraform_role" {
  name = var.lambda_role_name

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

data "aws_iam_policy" "AWSLambdaBasicExecutionRole" {
    arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy" "SecretsManagerReadWrite" {
    arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}

resource "aws_iam_role_policy_attachment" "lambda-basic" {
  role       = "${aws_iam_role.cla_terraform_role.name}"
  policy_arn = "${data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn}"
}

resource "aws_iam_role_policy_attachment" "secrets-read-write" {
  role       = "${aws_iam_role.cla_terraform_role.name}"
  policy_arn = "${data.aws_iam_policy.SecretsManagerReadWrite.arn}"
}