# Make zip archive from the python source file

data "archive_file" "lambda_zip_file" {
  type        = "zip"
  output_path = "./lambda.zip"
  source {
    content = file("../lambda_function.py")
    filename = "lambda_function.py"
  }
}


resource "aws_lambda_function" "lambda" {
  filename      = "${data.archive_file.lambda_zip_file.output_path}"
  function_name = var.lambda_function_name
  role          = aws_iam_role.cla_terraform_role.arn
  handler       = var.lambda_handler_name
  runtime       = "python3.6"

  environment {
      variables = {
          SECRET_ID = var.secrets_name
      }
  }

  source_code_hash = "${data.archive_file.lambda_zip_file.output_base64sha256}"
}

resource "aws_lambda_permission" "allow-api-gateway" {
  statement_id  = "AllowAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}
