terraform {
    required_providers {
        hashicorp-aws = {
            source  = "hashicorp/aws"
            version = ">= 3.42.0"
        }
        hashicorp-archive = {
            source  = "hashicorp/archive"
            version = ">= 2.2.0"
        }
    }
}

provider "hashicorp-aws" {
    region  = "us-east-2"
}

provider "aws" {
    region  = "us-east-2"
}
