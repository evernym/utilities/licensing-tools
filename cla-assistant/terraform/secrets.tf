variable "x_gitlab_token" {
    type = string
    description = "Please, specify X-Gitlab-Token"

    validation {
    condition     = length(var.x_gitlab_token) > 0
    error_message = "X-Gitlab-Token should be set and non-empty string."
  }
}

variable "gitlab_api_token" {
    type = string
    description = "Please, specify Gitlab-API-Token"

    validation {
        condition     = length(var.gitlab_api_token) > 0
        error_message = "Gitlab-Api-Token should be set and non-empty string."
    }
}

resource "aws_secretsmanager_secret" "gitlab_secrets" {
  name = var.secrets_name
}

resource "aws_secretsmanager_secret_version" "secrets_version" {
  secret_id     = aws_secretsmanager_secret.gitlab_secrets.id
  secret_string = jsonencode({
      "X-GitLab-Token" = "${var.x_gitlab_token}"
      "GitLab-API-Token" = "${var.gitlab_api_token}"
  })
}
