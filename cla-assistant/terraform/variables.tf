# Lambda constants

variable "lambda_function_name" {
    type        = string
    default     = "cla-terraform"
    description = "Name of AWS Lambda function"
}

variable "lambda_handler_name" {
    type        = string
    default     = "lambda_function.lambda_handler"
    description = "Name of Lambda handler function, which will be used for handling input requests"
}

# Secrets Manager constants

variable "secrets_name" {
    type        = string
    default     = "gitlab/cla-ter"
    description = "Name of secrets which will be used for keeping gitlab tokens"
}

# IAM contsants

variable "lambda_role_name" {
    type        = string
    default     = "gitlab-cla-lambda-role"
    description = "Name of role for Lambda function"
}

# API Gateway constants

variable "api_gateway_name" {
    type        = string
    default     = "cla-api-terraform"
    description = "Name for API Gateway"
}

variable "default_stage_name" {
    type        = string
    default     = "default"
    description = "Name of default stage for API Gateway"
}